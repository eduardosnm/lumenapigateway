<?php


namespace App\Services;


use App\Traits\ConsumeExternalService;

class AuthorService
{
    use ConsumeExternalService;

    /**
     * La url base del servicio de autores que se va a consumir
     * @var string
     */
    public $baseUri;

    /**
     * El hash secreto que sera usado para consumir el servicio de autores
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.authors.base_uri');
        $this->secret = config('services.authors.secret');
    }

    /**
     * Obtiene la lista completa del servicio autores
     * @return string
     */
    public function obtainAuthors()
    {
        return $this->performRequest("GET", '/authors');
    }

    /**
     * Crea una instancia de autor utilizando el servicio de autores
     * @param array $data
     * @return string
     */
    public function createAuthors(array $data)
    {
        return $this->performRequest("POST", "/authors",$data);
    }

    /**
     * Obtiene un unico autor del servicio autor
     * @param $author
     * @return string
     */
    public function obtainAuthor($author)
    {
        return $this->performRequest("GET", "/authors/{$author}");
    }

    /**
     * Actualiza un unico autor desde el servicio de autores
     * @param array $data
     * @param $author
     * @return string
     */
    public function editAuthor(array $data, $author)
    {
        return $this->performRequest("PUT", "/authors/{$author}", $data);
    }

    /**
     * Elimina un autor desde el servicio de autores
     * @param $author
     * @return string
     */
    public function deleteAuthor($author)
    {
        return $this->performRequest("DELETE", "/authors/{$author}");
    }

}
