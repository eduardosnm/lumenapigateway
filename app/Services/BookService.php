<?php


namespace App\Services;


use App\Traits\ConsumeExternalService;

class BookService
{
    use ConsumeExternalService;

    /**
     * La url base del servicio de autores que se va a consumir
     * @var string
     */
    public $baseUri;

    /**
     * El hash secreto que sera usado para consumir el servicio de autores
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.books.base_uri');
        $this->secret = config('services.books.secret');
    }

    /**
     * Obtiene la lista completa del servicio libros
     * @return string
     */
    public function obtainBooks()
    {
        return $this->performRequest("GET", '/books');
    }

    /**
     * Crea una instancia de libro utilizando el servicio de libros
     * @param array $data
     * @return string
     */
    public function createBooks(array $data)
    {
        return $this->performRequest("POST", "/books",$data);
    }

    /**
     * Obtiene un unico libro del servicio libros
     * @param $books
     * @return string
     */
    public function obtainBook($books)
    {
        return $this->performRequest("GET", "/books/{$books}");
    }

    /**
     * Actualiza un unico libro desde el servicio de libros
     * @param array $data
     * @param $books
     * @return string
     */
    public function editBook(array $data, $books)
    {
        return $this->performRequest("PUT", "/books/{$books}", $data);
    }

    /**
     * Elimina un libro desde el servicio de libros
     * @param $books
     * @return string
     */
    public function deleteBook($books)
    {
        return $this->performRequest("DELETE", "/books/{$books}");
    }
}
